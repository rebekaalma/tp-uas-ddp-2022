package TPUAS;

public class MataKuliah {
    private String nama;
    private String kode;
    private Mahasiswa[] daftarMhs;
    private int jumlahMhs;

    public MataKuliah(String nama, String kode) {
        this.nama = nama;
        this.kode = kode;
        this.daftarMhs = new Mahasiswa[2];
        this.jumlahMhs = 0;
    }

    public void tambahMhs(Mahasiswa mhs) {
        if (jumlahMhs == daftarMhs.length) {
            tambahUkurandaftarMhs();
        }

        daftarMhs[jumlahMhs] = mhs;
        jumlahMhs++;
    }

    private void tambahUkurandaftarMhs() {
        Mahasiswa[] temp = new Mahasiswa[daftarMhs.length * 2];
        System.arraycopy(daftarMhs, 0, temp, 0, daftarMhs.length);
        daftarMhs = temp;
    }

    public void dropMhs(Mahasiswa mhs) {
        for (int i = 0; i < jumlahMhs; i++) {
            if (daftarMhs[i].equals(mhs)) {
                for (int k = i + 1; k < jumlahMhs; k++) {
                    daftarMhs[k - 1] = daftarMhs[k];
                }

                jumlahMhs--;
                break;
            }
        }
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "Mata Kuliah dengan nama " + nama + " dan kode " + kode;
    }

    public String getNama() {
        return nama;
    }

    public String getKode() {
        return kode;
    }
}
